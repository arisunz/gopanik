// Limited capacity deques to hold keyboard input

package keydeque

import "strings"

type KeyDeque struct {
	capacity uint64
	deque    []string
}

// Return a fresh deque with the given size
func MakeDeque(size uint64) KeyDeque {
	return KeyDeque{size, make([]string, size, size)}
}

// Push a string onto a deque
func (kd *KeyDeque) KeyPush(str string) {
	if len(kd.deque) == cap(kd.deque) {
		temp := kd.deque[1:]
		copy(kd.deque, temp)
		kd.deque[len(kd.deque)-1] = str
	} else {
		kd.deque[len(kd.deque)] = str
	}
}

// Get the string representation of a deque
func (kd *KeyDeque) String() string {
	return strings.Join(kd.deque, "")
}

// Empty a deque
func (kd *KeyDeque) Flush() {
	kd.deque = make([]string, kd.capacity, kd.capacity)
}

// Check if a keydeque contains a given string
func (kd *KeyDeque) Contains(str string) bool {
	return strings.Contains(kd.String(), str)
}
