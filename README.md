# gopanik
Go port of [w3panik](https://gitlab.com/ariSun/w3panik).

`gopanic` sits in the background listening for certain keyboard sequences (specified by the user) and, upon detecting one, it will react by calling its corresponding shell command (also specified by the user). This is achieved by reading `/dev/input/event*` into a (small) buffer. Because of this and the nature of `gopanik`, it must be executed with root privileges.

Although multipurpose, this program is developed with critical and delicate situations in mind. If you're looking for a shortcut daemon, you're better off with an actual shortcut daemon, because this is not it.


## Usage
First, find out your keyboard event file by running `ls -l /dev/input/by-path`. You'll see files pointing to `../event*`. The one containing "keyboard" or "kbd" should be pointing to what you need.

Then you need to edit the configuration file (which should be located at `/etc/gopanik/gopanik.cfg` or specified with the `-c` option) with `input-device` set to the number corresponding to your keyboard's input device. It is recommended you use [the provided config file](gopanik.cfg) as an example:

```toml
## Example configuration
# this is a comment

[options] # this is a comment next to a section declaration
input-device = 0
buffer-size = 16

[commands]
testing = notify-send hello, there! # and this is a comment next to a command declaration
updateapt = /some/update/script # commands get executed as root
poweroffnow = poweroffcommand # needless to say, you should be careful with these
```


## Install
The quick and dirty way:

```
$ go install gitlab.com/ariSun/gopanik
```
Create a config file as above and you're good to go with `sudo gopanik &` or `sudo gopanik -c /path/to/config.cfg`.



Ye olde way, with a .service file:

```
$ git clone https://gitlab.com/ariSun/gopanik
$ cd gopanik
$ make

## install to /usr/bin/
# make install

## install and use systemd .service file
# make install-service

## and enable
# systemd enable --now gopanik
```

## License
(C) 2020 - Ariela Wenner

This project is licensed under the GNU GPLv3. See the [license](COPYING.md) for details.
