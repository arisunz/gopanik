// Abstraction for reading struct input_events from an event file

package inputevent

import (
	"bytes"
	"encoding/binary"
	"os"
)

type InputEvent struct {
	Code  uint16
	Value int32
}

// Read a single input event from file
func ReadInputEvent(file *os.File) InputEvent {
	tmp := make([]byte, 24)
	file.Read(tmp)

	var val int32
	binary.Read(bytes.NewReader(tmp[20:]), binary.LittleEndian, &val)
	code := binary.LittleEndian.Uint16(tmp[18:20])

	return InputEvent{code, val}
}
