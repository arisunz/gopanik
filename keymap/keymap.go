// Mapping of key codes from struct input_event
// We can eventually support multiple keyboard layouts by adding them here

package keymap

type KeyMap map[uint16]string

func MakeQwertyMapping() KeyMap {
	return map[uint16]string{
		2:  "1",
		3:  "2",
		4:  "3",
		5:  "4",
		6:  "5",
		7:  "6",
		8:  "7",
		9:  "8",
		10: "9",
		11: "0",
		12: "-",
		13: "=",
		16: "q",
		17: "w",
		18: "e",
		19: "r",
		20: "t",
		21: "y",
		22: "u",
		23: "i",
		24: "o",
		25: "p",
		30: "a",
		31: "s",
		32: "d",
		33: "f",
		34: "g",
		35: "h",
		36: "j",
		37: "k",
		38: "l",
		39: ";",
		40: "'",
		41: "`",
		43: "\\",
		44: "z",
		45: "x",
		46: "c",
		47: "v",
		48: "b",
		49: "n",
		50: "m",
		51: ",",
		52: ".",
		53: "/",
	}
}
