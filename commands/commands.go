// Commands
// Simple abstraction for key-value pairs of shell commands

package commands

import (
	"os/exec"
	"strings"

	"gitlab.com/ariSun/gopanik/keydeque"
)

type Commands map[string]string

// Traverse a Commands map to find a match with the given KeyDeque
func (cmds Commands) HasMatch(kd keydeque.KeyDeque) (bool, string) {
	for k, v := range cmds {
		if kd.Contains(k) {
			return true, v
		}
	}

	return false, ""
}

// Build and run a Cmd
func Execute(cmdstr string) {
	cmdSlice := strings.Split(cmdstr, " ")
	var cmd *exec.Cmd

	if len(cmdSlice) > 1 {
		cmd = exec.Command(cmdSlice[0], cmdSlice[1:]...)
	} else {
		cmd = exec.Command(cmdSlice[0])
	}

	go cmd.Run()
}
