
make: gopanik.go
	go build gopanik.go

install: gopanik
	mkdir -p /etc/gopanik
	install -m 644 gopanik.cfg /etc/gopanik/
	install gopanik /usr/bin/

uninstall:
	rm -rf /etc/gopanik
	rm -f /usr/bin/gopanik
	rm -f /etc/systemd/system/gopanik.service

install-service: gopanik.service
	install gopanik.service /etc/systemd/system/
