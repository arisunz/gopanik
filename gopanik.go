package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strconv"
	"syscall"

	"gitlab.com/ariSun/gopanik/commands"
	"gitlab.com/ariSun/gopanik/configparse"
	"gitlab.com/ariSun/gopanik/inputevent"
	"gitlab.com/ariSun/gopanik/keydeque"
	"gitlab.com/ariSun/gopanik/keymap"
)

const (
	DEFAULT_BUFFER_SIZE uint64 = 16
	DEFAULT_CONFIG_PATH string = "/etc/gopanik/gopanik.cfg"
)

func getConfigPath() string {
	mc := flag.String("c", DEFAULT_CONFIG_PATH, "-c=/config/file/path.cfg")
	flag.Parse()
	return *mc
}

func main() {
	log.SetOutput(os.Stdout)

	if os.Getuid() != 0 {
		log.Fatal("This program must be run as root!")
	}

	configPath := getConfigPath()

	options := configparse.GetSection(configPath, "options")

	nev := options.GetConfig("input-device")
	if nev == "" {
		log.Fatal("Must specify an input device!")
	}

	f, err := os.Open("/dev/input/event" + nev)
	if err != nil {
		fmt.Println("Could not open event device. Is the device number valid?")
		log.Fatal(err)
	}
	defer f.Close()

	var bufferLength uint64
	bufferLengthConfig := options.GetConfig("buffer-size")
	if bufferLengthConfig == "" {
		bufferLength = DEFAULT_BUFFER_SIZE
	} else {
		n, err := strconv.ParseUint(bufferLengthConfig, 10, 64)
		if err != nil {
			bufferLength = DEFAULT_BUFFER_SIZE
		} else {
			bufferLength = n
		}
	}

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		select {
		case <-signalChan:
			log.Printf("Got termination signal, shutting down.")
			os.Exit(1)
		}
	}()

	var cmds commands.Commands = configparse.GetSection(configPath, "commands").Configs
	keymap := keymap.MakeQwertyMapping()
	deque := keydeque.MakeDeque(bufferLength)

	for {
		event := inputevent.ReadInputEvent(f)
		if event.Value == 1 && keymap[event.Code] != "" {
			deque.KeyPush(keymap[event.Code])
			ok, cmd := cmds.HasMatch(deque)
			if ok {
				commands.Execute(cmd)
				deque.Flush()
			}
		}
	}
}
