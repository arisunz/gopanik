// Simple parsing of TOML-like config files

package configparse

import (
	"bufio"
	"log"
	"os"
	"strings"
)

type ConfigSection struct {
	name    string
	Configs map[string]string
}

func isSection(str string) bool {
	tokens := strings.Split(str, " ")
	return str != "" &&
		len(tokens) == 1 &&
		string(str[0]) == "[" &&
		string(str[len(str)-1]) == "]"
}

func isConfig(str string) (bool, string, string) {
	tokens := strings.Split(str, "=")
	if len(tokens) == 2 {
		return true, strings.TrimSpace(tokens[0]), strings.TrimSpace(tokens[1])
	} else {
		return false, "", ""
	}
}

func trimComment(str string) string {
	for n, c := range str {
		if c == '#' {
			return str[:n]
		}
	}

	return str
}

// Fetch a ConfigSection with a given name from the config file pointed in path
func GetSection(path string, name string) ConfigSection {
	log.SetOutput(os.Stdout)
	cfg, err := os.Open(path)
	if err != nil {
		log.Fatal(err)
	}
	defer cfg.Close()

	section := ConfigSection{name, make(map[string]string)}

	scanner := bufio.NewScanner(cfg)
	for scanner.Scan() {
		line := strings.TrimSpace(trimComment(scanner.Text()))
		if isSection(line) && strings.Contains(line, name) {
			for scanner.Scan() {
				line := strings.TrimSpace(trimComment(scanner.Text()))
				ok, k, v := isConfig(line)
				if ok {
					section.Configs[k] = v
				} else if isSection(line) {
					return section
				}
			}
		}
	}

	return section
}

// Get the config matching key, or an empty string if there's no match
func (section ConfigSection) GetConfig(key string) string {
	return section.Configs[key]
}
